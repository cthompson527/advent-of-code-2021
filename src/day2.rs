use std::str::FromStr;

#[derive(Eq, PartialEq, Debug)]
enum Direction {
    Forward,
    Up,
    Down
}

#[derive(Eq, PartialEq, Debug)]
struct Movement {
    direction: Direction,
    amount: i32
}

fn parse_direction(d: &str) -> Direction {
    match d {
        "forward" => Direction::Forward,
        "down" => Direction::Down,
        "up" => Direction::Up,
        _ => panic!("bad direction")
    }
}

fn parse_lines(input: &[String]) -> impl Iterator<Item=Movement> + '_ {
    input
        .iter()
        .map(|val| val.split(" ").collect())
        .map(|elements: Vec<&str>| (parse_direction(elements[0]), i32::from_str(elements[1]).unwrap()))
        .map(|pair| Movement { direction: pair.0, amount: pair.1 })
}

fn add_movement(old_position: (i32, i32), m: Movement) -> (i32, i32) {
    match m.direction {
        Direction::Up   => (old_position.0, old_position.1 - m.amount),
        Direction::Down => (old_position.0, old_position.1 + m.amount),
        Direction::Forward => (old_position.0 + m.amount, old_position.1),
    }
}

fn add_movement_part_2(old_position: (i32, i32, i32), m: Movement) -> (i32, i32, i32) {
    let (horizontal, depth, aim) = old_position;
    match m.direction {
        Direction::Up   => (horizontal, depth, aim - m.amount),
        Direction::Down => (horizontal, depth, aim + m.amount),
        Direction::Forward => (horizontal + m.amount, depth + (aim * m.amount), aim),
    }
}

pub fn part_one(input: &[String]) -> i32 {
    let movements = parse_lines(input).fold((0, 0), add_movement);
    movements.0 * movements.1
}

pub fn part_two(input: &[String]) -> i32 {
    let (vert, depth, _aim) = parse_lines(input).fold((0, 0, 0), add_movement_part_2);
    vert * depth
}

#[cfg(test)]
mod tests {
    use super::{Direction, parse_direction, parse_lines, add_movement, Movement, part_one, add_movement_part_2, part_two};

    fn sample_input() -> Vec<String> {
        vec![
            "forward 5".to_string(),
            "down 5".to_string(),
            "forward 8".to_string(),
            "up 3".to_string(),
            "down 8".to_string(),
            "forward 2".to_string(),
        ]
    }

    #[test]
    fn test_parse_lines() {
        let lines: Vec<Movement> = parse_lines(sample_input().as_slice()).collect();
        assert_eq!(lines, vec![
            Movement{ direction: Direction::Forward, amount: 5 },
            Movement{ direction: Direction::Down, amount: 5 },
            Movement{ direction: Direction::Forward, amount: 8 },
            Movement{ direction: Direction::Up, amount: 3 },
            Movement{ direction: Direction::Down, amount: 8 },
            Movement{ direction: Direction::Forward, amount: 2 },
        ])
    }

    #[test]
    fn test_parse_direction() {
        assert_eq!(parse_direction("forward"), Direction::Forward);
        assert_eq!(parse_direction("up"), Direction::Up);
        assert_eq!(parse_direction("down"), Direction::Down);
    }

    #[test]
    fn test_add_movement_updates_the_new_movement() {
        assert_eq!(add_movement((0, 0), Movement { direction: Direction::Forward, amount: 2 }), (2, 0));
        assert_eq!(add_movement((2, 4), Movement { direction: Direction::Down, amount: 2 }), (2, 6));
        assert_eq!(add_movement((2, 6), Movement { direction: Direction::Up, amount: 2 }), (2, 4));
    }

    #[test]
    fn test_part_one_aggregates_the_movements() {
        assert_eq!(part_one(sample_input().as_slice()), 150);
    }

    #[test]
    fn test_add_movement_part_2_updates_the_new_movement() {
        assert_eq!(add_movement_part_2((0, 0, 0), Movement { direction: Direction::Forward, amount: 2 }), (2, 0, 0));
        assert_eq!(add_movement_part_2((2, 4, 0), Movement { direction: Direction::Down, amount: 2 }), (2, 4, 2));
        assert_eq!(add_movement_part_2((2, 6, 0), Movement { direction: Direction::Up, amount: 2 }), (2, 6, -2));
        assert_eq!(add_movement_part_2((2, 6, 2), Movement { direction: Direction::Forward, amount: 2 }), (4, 10, 2));
    }

    #[test]
    fn test_part_two_aggregates_the_movements() {
        assert_eq!(part_two(sample_input().as_slice()), 900);
    }
}
