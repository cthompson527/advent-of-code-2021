use std::borrow::BorrowMut;
use regex::Regex;

#[derive(Clone, Copy)]
struct Board {
    numbers: [[i32; 5]; 5],
    active: [[i32; 5]; 5],
}

fn parse(input: &[String]) -> (Vec<i32>, Vec<Board>) {
    let selections = input[0]
        .split(',')
        .map(|i| i.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();
    let re = Regex::new(r"^ ?(\d{1,2}) {1,2}(\d{1,2}) {1,2}(\d{1,2}) {1,2}(\d{1,2}) {1,2}(\d{1,2})$").unwrap();
    let mut peek_input = input.into_iter().skip(1).peekable();
    let mut boards = vec![];

    while peek_input.peek().is_some() {
        let chunk: Vec<&String> = peek_input
            .by_ref()
            .skip(1)
            .take(5)
            .collect();

        let mut numbers = [[0 as i32; 5]; 5];
        let active = numbers.clone();

        for (index, line) in chunk.iter().enumerate() {
            for cap in re.captures_iter(line) {
                for i in 0..5 {
                    numbers[index][i] = (&cap[i + 1]).parse().unwrap();
                }
            }
        }

        boards.push(Board{ numbers, active });
    }

    (selections, boards)
}

fn check_rows(active: [[i32; 5]; 5]) -> Option<usize> {
    active
        .into_iter()
        .map(|x| x.iter().sum())
        .position(|x: i32| x == 5)
}

fn check_cols(active: [[i32; 5]; 5]) -> Option<usize> {
    let transposed = transpose(active);
    check_rows(transposed)
}

fn transpose(values: [[i32; 5]; 5]) -> [[i32; 5]; 5] {
    let mut transposed = [[0 as i32; 5]; 5];
    for (x, row) in values.into_iter().enumerate() {
        for (y, val) in row.into_iter().enumerate() {
            transposed[y][x] = val;
        }
    }
    transposed
}

fn set_active(board: &mut Board, num: i32) {
    let numbers = board.numbers;
    for y in 0..numbers.len() {
        for x in 0..numbers[y].len() {
            if numbers[y][x] == num {
                board.active[y][x] = 1;
            }
        }
    }
}

fn check_grid(active: [[i32; 5]; 5]) -> Option<(usize, bool)> {
    let row = match check_rows(active) {
        None => None,
        Some(v) => Some((v, false)),
    };
    if row.is_some() {
        return row;
    }

    match check_cols(active) {
        None => None,
        Some(v) => Some((v, true)),
    }
}

fn play_game(selections: Vec<i32>, mut boards: Vec<Board>) -> (i32, Board) {
    for selection in selections {
        for board_index in 0..boards.len() {
            let board = boards[board_index].borrow_mut();
            set_active(board, selection);
            let complete = check_grid(board.clone().active);
            if complete.is_some() {
                return (selection, *board);
            }
        }
    }
    (-1, Board{ numbers: [[0 as i32; 5]; 5], active: [[0 as i32; 5]; 5] })
}

fn sum_unmarked_numbers(numbers: [[i32; 5]; 5], active: [[i32; 5]; 5]) -> i32 {
    let mut sum = 0;
    for (y, row) in numbers.iter().enumerate() {
        for (x, val) in row.iter().enumerate() {
            if active[y][x] == 0 {
                sum += val;
            }
        }
    }
    sum
}

fn play_game_for_last(selections: Vec<i32>, mut boards: Vec<Board>) -> (i32, Board) {
    let mut completed = vec![0; boards.len()];
    for selection in selections {
        for board_index in 0..boards.len() {
            let board = boards[board_index].borrow_mut();
            set_active(board, selection);
            let complete = check_grid(board.clone().active);
            if complete.is_some() {
                completed[board_index] = 1;
                if completed.iter().sum::<i32>() == completed.len() as i32 {
                    return (selection, *board);
                }
            }
        }
    }
    (-1, Board{ numbers: [[0 as i32; 5]; 5], active: [[0 as i32; 5]; 5] })
}


pub fn part_one(input: &[String]) -> i32 {
    let (selections, boards) = parse(input);
    let (winning_num, board) = play_game(selections, boards);
    let sum = sum_unmarked_numbers(board.numbers, board.active);
    sum * winning_num
}

pub fn part_two(input: &[String]) -> i32 {
    let (selections, boards) = parse(input);
    let (winning_num, board) = play_game_for_last(selections, boards);
    let sum = sum_unmarked_numbers(board.numbers, board.active);
    sum * winning_num
}

#[cfg(test)]
mod test {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec![
            "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1".to_string(),
                          "".to_string(),
            "22 13 17 11  0".to_string(),
            " 8  2 23  4 24".to_string(),
            "21  9 14 16  7".to_string(),
            " 6 10  3 18  5".to_string(),
            " 1 12 20 15 19".to_string(),
                          "".to_string(),
            " 3 15  0  2 22".to_string(),
            " 9 18 13 17  5".to_string(),
            "19  8  7 25 23".to_string(),
            "20 11 10 24  4".to_string(),
            "14 21 16 12  6".to_string(),
                          "".to_string(),
            "14 21 17 24  4".to_string(),
            "10 16 15  9 19".to_string(),
            "18  8 23 26 20".to_string(),
            "22 11 13  6  5".to_string(),
            " 2  0 12  3  7".to_string(),
        ]
    }

    #[test]
    fn test_parse_builds_the_input() {
        let (picks, boards) = parse(sample_input().as_slice());
        assert_eq!(picks, vec![7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1]);
        let active = [[0 as i32; 5]; 5];
        let board1 = [
            [22, 13, 17, 11,  0],
            [ 8,  2, 23,  4, 24],
            [21,  9, 14, 16,  7],
            [ 6, 10,  3, 18,  5],
            [ 1, 12, 20, 15, 19],
        ];
        let board2 = [
            [ 3, 15,  0,  2, 22],
            [ 9, 18, 13, 17,  5],
            [19,  8,  7, 25, 23],
            [20, 11, 10, 24,  4],
            [14, 21, 16, 12,  6],
        ];
        let board3 = [
            [14, 21, 17, 24,  4],
            [10, 16, 15,  9, 19],
            [18,  8, 23, 26, 20],
            [22, 11, 13,  6,  5],
            [ 2,  0, 12,  3,  7],
        ];
        let board1 = Board { numbers: board1, active };
        let board2 = Board { numbers: board2, active };
        let board3 = Board { numbers: board3, active };
        assert_eq!(boards[0].numbers, board1.numbers);
        assert_eq!(boards[1].numbers, board2.numbers);
        assert_eq!(boards[2].numbers, board3.numbers);
        assert_eq!(boards.len(), 3);
    }

    #[test]
    fn test_set_active_sets_the_active_num() {
        let numbers = [
            [22, 13, 17, 11,  0],
            [ 8,  2, 23,  4, 24],
            [21,  9, 14, 16,  7],
            [ 6, 10,  3, 18,  5],
            [ 1, 12, 20, 15, 19],
        ];
        let active = [[0 as i32; 5]; 5];
        let mut board = Board { numbers, active };
        set_active(&mut board, 23);
        assert_eq!(board.active[1][2], 1);
    }

    #[test]
    fn test_check_rows_returns_index_of_complete_row() {
        let numbers = [
            [22, 13, 17, 11,  0],
            [ 8,  2, 23,  4, 24],
            [21,  9, 14, 16,  7],
            [ 6, 10,  3, 18,  5],
            [ 1, 12, 20, 15, 19],
        ];
        let active = [[0 as i32; 5]; 5];
        let mut board = Board { numbers, active };
        assert_eq!(check_rows(board.active), None);
        set_active(&mut board, 21);
        set_active(&mut board, 9);
        set_active(&mut board, 14);
        set_active(&mut board, 16);
        set_active(&mut board, 7);
        assert_eq!(check_rows(board.active), Some(2));
    }

    #[test]
    fn test_check_cols_returns_index_of_complete_column() {
        let numbers = [
            [22, 13, 17, 11,  0],
            [ 8,  2, 23,  4, 24],
            [21,  9, 14, 16,  7],
            [ 6, 10,  3, 18,  5],
            [ 1, 12, 20, 15, 19],
        ];
        let active = [[0 as i32; 5]; 5];
        let mut board = Board { numbers, active };
        assert_eq!(check_cols(board.active), None);
        set_active(&mut board, 11);
        set_active(&mut board, 4);
        set_active(&mut board, 16);
        set_active(&mut board, 18);
        set_active(&mut board, 15);
        assert_eq!(check_cols(board.active), Some(3));
    }

    #[test]
    fn test_check_grid_returns_the_index_and_whether_it_is_row_or_col() {
        let numbers = [
            [22, 13, 17, 11,  0],
            [ 8,  2, 23,  4, 24],
            [21,  9, 14, 16,  7],
            [ 6, 10,  3, 18,  5],
            [ 1, 12, 20, 15, 19],
        ];
        let active = [[0 as i32; 5]; 5];
        let board = Board { numbers, active };
        assert_eq!(check_grid(board.active), None);
        let mut board = Board { numbers, active };
        set_active(&mut board, 21);
        set_active(&mut board, 9);
        set_active(&mut board, 14);
        set_active(&mut board, 16);
        set_active(&mut board, 7);
        assert_eq!(check_grid(board.active), Some((2, false)));
        let mut board = Board { numbers, active };
        set_active(&mut board, 11);
        set_active(&mut board, 4);
        set_active(&mut board, 16);
        set_active(&mut board, 18);
        set_active(&mut board, 15);
        assert_eq!(check_grid(board.active), Some((3, true)));
    }

    #[test]
    fn test_play_game() {
        let (selections, boards) = parse(sample_input().as_slice());
        let (winning_num, board) = play_game(selections, boards);
        assert_eq!(winning_num, 24);
        assert_eq!(board.active, [
            [1, 1, 1, 1, 1],
            [0, 0, 0, 1, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 1],
            [1, 1, 0, 0, 1],
        ]);
    }

    #[test]
    fn test_sum_unmarked_numbers() {
        let (selections, boards) = parse(sample_input().as_slice());
        let (_winning_num, board) = play_game(selections, boards);
        assert_eq!(sum_unmarked_numbers(board.numbers, board.active), 188);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input().as_slice()), 4512);
    }

    #[test]
    fn test_sum_unmarked_numbers_for_last() {
        let (selections, boards) = parse(sample_input().as_slice());
        let (_winning_num, board) = play_game_for_last(selections, boards);
        assert_eq!(sum_unmarked_numbers(board.numbers, board.active), 148);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input().as_slice()), 1924);
    }
}
