fn parse(input: &[String]) -> Vec<i32> {
    input[0].split(",").map(|x| x.parse::<i32>().unwrap()).collect()
}

fn next_generation(vals: Vec<i32>) -> Vec<i32> {
    let mut add = 0;
    let vals: Vec<i32> = vals
        .into_iter()
        .map(|val| if val > 0 { val - 1 } else { add += 1; 6 })
        .collect();
    [vals, vec![8; add]].concat()
}

fn iterate(vals: Vec<i32>, iteration: i32, stop: i32) -> Vec<i32> {
    if iteration == stop {
        return vals;
    }
    iterate(next_generation(vals.clone()), iteration + 1, stop)
}

fn iterate_counters(days: Vec<i64>, iteration: i32, stop: i32) -> Vec<i64> {
    if iteration == stop {
        return days;
    }
    iterate_counters(next_generation_2(days.clone()), iteration + 1, stop)
}

fn next_generation_2(vals: Vec<i64>) -> Vec<i64> {
    let mut new_days = vec![0; vals.len()];
    for (index, val) in vals.into_iter().enumerate() {
        if index == 0 {
            new_days[8] = val;
        } else {
            new_days[index-1] = val;
        }
    }
    new_days[6] += new_days[8];
    new_days
}

fn day_counter(vals: Vec<i32>) -> Vec<i64> {
    let mut day_counter = vec![0; 9];
    for val in vals {
        day_counter[val as usize] += 1;
    }
    day_counter
}

pub fn part_one(input: &[String]) -> i32 {
    let vals = parse(input);
    iterate(vals, 0, 80).len() as i32
}

pub fn part_two(input: &[String]) -> i64 {
    let vals = parse(input);
    let days = day_counter(vals);
    let days = iterate_counters(days, 0, 256);
    days.into_iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec!["3,4,3,1,2".to_string()]
    }

    #[test]
    fn test_parse_input() {
        let parsed = parse(sample_input().as_slice());
        assert_eq!(parsed, vec![3,4,3,1,2]);
    }

    #[test]
    fn test_next_generation_returns_the_next_iteration_of_the_vals() {
        let parsed = parse(sample_input().as_slice());
        let next = next_generation(parsed);
        assert_eq!(next, vec![2,3,2,0,1]);
        let next = next_generation(next);
        assert_eq!(next, vec![1,2,1,6,0,8]);
        let next = next_generation(next);
        assert_eq!(next, vec![0,1,0,5,6,7,8]);
    }

    #[test]
    fn test_iterate_will_run_over_generations() {
        let parsed = parse(sample_input().as_slice());
        let vals = iterate(parsed.clone(), 0, 18);
        assert_eq!(vals.len(), 26);
        let vals = iterate(parsed.clone(), 0, 80);
        assert_eq!(vals.len(), 5934);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input().as_slice()), 5934);
    }

    #[test]
    fn test_day_counter() {
        let parsed = parse(sample_input().as_slice());
        let days = day_counter(parsed);
        assert_eq!(days, vec![0, 1, 1, 2, 1, 0, 0, 0, 0]);
    }

    #[test]
    fn test_next_generation_2() {
        let parsed = parse(sample_input().as_slice());
        let days = day_counter(parsed);
        assert_eq!(days, vec![0, 1, 1, 2, 1, 0, 0, 0, 0]);
        let next = next_generation_2(days);
        assert_eq!(next, vec![1, 1, 2, 1, 0, 0, 0, 0, 0]);
        let next = next_generation_2(next);
        assert_eq!(next, vec![1, 2, 1, 0, 0, 0, 1, 0, 1]);
        let next = next_generation_2(next);
        assert_eq!(next, vec![2, 1, 0, 0, 0, 1, 1, 1, 1]);
        let next = next_generation_2(next);
        assert_eq!(next, vec![1, 0, 0, 0, 1, 1, 3, 1, 2]);
    }

    #[test]
    fn test_iterate_counters() {
        let parsed = parse(sample_input().as_slice());
        let days = day_counter(parsed);
        let days = iterate_counters(days, 0, 4);
        assert_eq!(days, vec![1, 0, 0, 0, 1, 1, 3, 1, 2]);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input().as_slice()), 26984457539);
    }
}
