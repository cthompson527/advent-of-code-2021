use std::collections::HashSet;
use itertools::Itertools;

fn output_values(input: Vec<String>) -> Vec<String> {
    input.iter().map(|i| i.split(" | ").skip(1).next().unwrap().to_string()).collect::<Vec<String>>()
}

fn input_values(input: Vec<String>) -> Vec<String> {
    input.iter().map(|i| i.split(" | ").next().unwrap().to_string()).collect::<Vec<String>>()
}

fn count_easy_numbers(input: Vec<String>) -> i32 {
    input
        .iter()
        .flat_map(|line| line.split(" "))
        .fold(0, |acc, cur| if cur.len() != 5 && cur.len() != 6 { acc + 1 } else { acc })
}

fn find_easy_nums(input: String) -> (String, String, String, String) {
    input
        .split(" ")
        .filter(|val| val.len() != 5 && val.len() != 6)
        .sorted_by(|a, b| Ord::cmp(&a.len(), &b.len()))
        .map(|v| v.to_string())
        .collect_tuple()
        .unwrap()
}

fn find_nine(possible: String, four: String, seven: String) -> String {
    let four: HashSet<char> = four
        .chars()
        .collect();
    let seven: HashSet<char> = seven
        .chars()
        .collect();
    let combined: HashSet<_> = four.union(&seven).copied().collect();

    possible
        .split(" ")
        .filter(|v| v.len() == 6)
        .map(|v| (v.clone(), v.chars().collect::<HashSet<char>>()))
        .filter(|(_, h)| h.difference(&combined).collect_vec().len() == 1)
        .map(|(orig_str, _)| orig_str.to_string())
        .collect_vec()[0].clone()
}

fn find_zero(remaining: Vec<String>, one: String) -> String {
    let one: HashSet<char> = one
        .chars()
        .collect();

    remaining
        .into_iter()
        .map(|v| (v.clone(), v.chars().collect::<HashSet<char>>()))
        .filter(|(_, h)| one.difference(&h).collect_vec().len() == 0)
        .map(|(orig_str, _) | orig_str)
        .collect_vec()[0].clone()
}

fn find_three(remaining: Vec<String>, one: String) -> String {
    let one: HashSet<char> = one
        .chars()
        .collect();

    remaining
        .into_iter()
        .map(|v| (v.clone(), v.chars().collect::<HashSet<char>>()))
        .filter(|(_, h)| one.difference(&h).collect_vec().len() == 0)
        .map(|(orig_str, _) | orig_str)
        .collect_vec()[0].clone()
}


fn find_five(remaining: Vec<String>, six: String) -> String {
    let six: HashSet<char> = six
        .chars()
        .collect();

    remaining
        .into_iter()
        .map(|v| (v.clone(), v.chars().collect::<HashSet<char>>()))
        .filter(|(_, h)| six.difference(&h).collect_vec().len() == 1)
        .map(|(orig_str, _) | orig_str)
        .collect_vec()[0].clone()
}

fn find_len_five(input: String, one: String, six: String) -> (String, String, String) {
    let remaining = input
        .split(" ")
        .filter(|s| s.len() == 5)
        .map(|s| s.to_string())
        .collect_vec();

    let three = find_three(remaining.clone(), one.clone());
    let remaining = remaining.into_iter().filter(|s| *s != three).collect_vec();
    let five = find_five(remaining.clone(), six.clone());
    let two = if remaining[0] == five { remaining[1].clone() } else { remaining[0].clone() };
    (two, three, five)
}

fn find_len_six(input: String, one: String, four: String, seven: String) -> (String, String, String) {
    let nine = find_nine(input.clone(), four.clone(), seven.clone());

    let remaining = input
        .split(" ")
        .filter(|s| s.len() == 6 && s.to_string() != nine)
        .map(|s| s.to_string())
        .collect_vec();

    let zero = find_zero(remaining.clone(), one);
    let six = if remaining[0] == zero { remaining[1].clone() } else { remaining[0].clone() };
    (zero, six, nine)
}

fn get_digit(output_val: String, sets: &[HashSet<char>; 6]) -> i32 {
    if output_val.len() == 2 { return 1 };
    if output_val.len() == 3 { return 7 };
    if output_val.len() == 4 { return 4 };
    if output_val.len() == 7 { return 8 };
    let output_val: HashSet<char> = output_val.chars().collect();
    if output_val.symmetric_difference(&sets[0]).collect_vec().len() == 0 { return 0 };
    if output_val.symmetric_difference(&sets[1]).collect_vec().len() == 0 { return 6 };
    if output_val.symmetric_difference(&sets[2]).collect_vec().len() == 0 { return 9 };
    if output_val.symmetric_difference(&sets[3]).collect_vec().len() == 0 { return 2 };
    if output_val.symmetric_difference(&sets[4]).collect_vec().len() == 0 { return 3 };
    5
}

fn output_number(input: String) -> i32 {
    let input_line = vec![input.clone()];
    let input_vals = input_values(input_line.clone())[0].clone();
    let output_vals = output_values(input_line.clone())[0].clone();
    let output_vals = output_vals.split(" ").collect_vec();
    let (one, four, seven, _eight) = find_easy_nums(input_vals.clone());
    let (zero, six, nine) = find_len_six(input_vals.clone(), one.clone(), four, seven);
    let (two, three, five) = find_len_five(input_vals.clone(), one, six.clone());
    let zero: HashSet<char> = zero.chars().collect();
    let six: HashSet<char> = six.chars().collect();
    let nine: HashSet<char> = nine.chars().collect();
    let two: HashSet<char> = two.chars().collect();
    let three: HashSet<char> = three.chars().collect();
    let five: HashSet<char> = five.chars().collect();
    let sets = [zero, six, nine, two, three, five];
    let mut number = get_digit(output_vals[0].to_string(), &sets) * 1000;
    number = number + get_digit(output_vals[1].to_string(), &sets) * 100;
    number = number + get_digit(output_vals[2].to_string(), &sets) * 10;
    number + get_digit(output_vals[3].to_string(), &sets)
}

pub fn part_one(input: Vec<String>) -> i32 {
    count_easy_numbers(output_values(input))
}

pub fn part_two(input: Vec<String>) -> i32 {
    input.into_iter().fold(0, |acc, cur| acc + output_number(cur))
}

#[cfg(test)]
mod tests {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec![
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe".to_string(),
            "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc".to_string(),
            "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg".to_string(),
            "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb".to_string(),
            "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea".to_string(),
            "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb".to_string(),
            "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe".to_string(),
            "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef".to_string(),
            "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb".to_string(),
            "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce".to_string(),
        ]
    }

    #[test]
    fn test_get_output_values() {
        let expected = vec![
            "fdgacbe cefdb cefbgd gcbe".to_string(),
            "fcgedb cgb dgebacf gc".to_string(),
            "cg cg fdcagb cbg".to_string(),
            "efabcd cedba gadfec cb".to_string(),
            "gecf egdcabf bgf bfgea".to_string(),
            "gebdcfa ecba ca fadegcb".to_string(),
            "cefg dcbef fcge gbcadfe".to_string(),
            "ed bcgafe cdgba cbgef".to_string(),
            "gbdfcae bgc cg cgb".to_string(),
            "fgae cfgab fg bagce".to_string(),
        ];
        assert_eq!(output_values(sample_input()), expected);
    }

    #[test]
    fn test_count_easy_numbers() {
        assert_eq!(count_easy_numbers(output_values(sample_input())), 26);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input()), 26);
    }

    #[test]
    fn test_input_values() {
        let expected = vec![
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb".to_string(),
            "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec".to_string(),
            "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef".to_string(),
            "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega".to_string(),
            "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga".to_string(),
            "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf".to_string(),
            "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf".to_string(),
            "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd".to_string(),
            "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg".to_string(),
            "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc".to_string(),
        ];
        assert_eq!(input_values(sample_input()), expected);
    }

    #[test]
    fn test_find_easy_nums() {
        let input = input_values(sample_input());
        let (one, seven, four, eight) = find_easy_nums(input[1].clone());
        assert_eq!(one, "gc");
        assert_eq!(seven, "cbg");
        assert_eq!(four, "gfec");
        assert_eq!(eight, "gcadebf");
    }

    #[test]
    fn test_find_nine() {
        let input = input_values(sample_input());
        let (_, four, seven, _) = find_easy_nums(input[0].clone());
        let nine = find_nine(input[0].clone(), four, seven);
        assert_eq!(nine, "cbdgef");
    }

    #[test]
    fn test_find_zero() {
        let input = input_values(sample_input());
        let (one, _, _, _) = find_easy_nums(input[0].clone());
        let zero = find_zero(vec!["agebfd".to_string(), "fgaecd".to_string()], one);
        assert_eq!(zero, "agebfd");
    }

    #[test]
    fn test_find_len_six() {
        let input = input_values(sample_input());
        let (one, four, seven, _) = find_easy_nums(input[0].clone());
        let (zero, six, nine) = find_len_six(input[0].clone(), one, four, seven);
        assert_eq!(nine, "cbdgef");
        assert_eq!(zero, "agebfd");
        assert_eq!(six, "fgaecd");
    }

    #[test]
    fn test_find_three() {
        let input = input_values(sample_input());
        let (one, _, _, _) = find_easy_nums(input[0].clone());
        let three = find_three(vec!["fdcge".to_string(), "fecdb".to_string(), "fabcd".to_string()], one);
        assert_eq!(three, "fecdb");
    }

    #[test]
    fn test_find_len_five() {
        let input = input_values(sample_input());
        let (one, four, seven, _eight) = find_easy_nums(input[0].clone());
        let (_zero, six, _nine) = find_len_six(input[0].clone(), one.clone(), four, seven);
        let (two, three, five) = find_len_five(input[0].clone(), one, six);
        assert_eq!(two, "fabcd");
        assert_eq!(three, "fecdb");
        assert_eq!(five, "fdcge");
    }

    #[test]
    fn test_output_number() {
        let input = sample_input();
        assert_eq!(output_number(input[0].clone()), 8394);
    }

    #[test]
    fn test_part_two() {
        let input = sample_input();
        assert_eq!(part_two(input), 61229);
    }
}