use itertools::Itertools;
use ndarray::{Array, Array2};

fn parse_input(input: Vec<String>) -> Vec<Vec<i32>> {
    input
        .into_iter()
        .map(|line| line.chars().into_iter()
            .map(|ch| ch.to_digit(10).unwrap() as i32).collect_vec()
        ).collect_vec()
}

fn is_smallest(input: &Array2<i32>, x_index: usize, y_index: usize) -> bool {
    let val = input[[y_index, x_index]];
    let size = input.shape();
    let [y_size, x_size] = [size[0], size[1]];
    if y_index > 0 && input[[y_index-1, x_index]] <= val { return false; }
    if y_index < y_size-1 && input[[y_index+1, x_index]] <= val { return false; }
    if x_index > 0 && input[[y_index, x_index-1]] <= val { return false; }
    if x_index < x_size-1 && input[[y_index, x_index+1]] <= val { return false; }
    return true;
}

fn count_smallest(input: Vec<Vec<i32>>) -> i32 {
    let arr = Array::from_shape_vec((input.len(), input[0].len()), input.into_iter().flatten().collect()).unwrap();
    let mut count = 0;
    let size = arr.shape();
    let [y_size, x_size] = [size[0], size[1]];
    for y_index in 0..y_size {
        for x_index in 0..x_size {
            if is_smallest(&arr, x_index, y_index) {
                count += arr[[y_index, x_index]] + 1;
            }
        }
    }
    count
}

fn find_bisons(input: Vec<Vec<i32>>) -> Vec<i32> {
    let arr = Array::from_shape_vec((input.len(), input[0].len()), input.into_iter().flatten().collect()).unwrap();
    let mut bisons = vec![];
    let size = arr.shape();
    let [y_size, x_size] = [size[0], size[1]];
    for y_index in 0..y_size {
        for x_index in 0..x_size {
            if is_smallest(&arr, x_index, y_index) {
                let shape = arr.shape();
                let mut reached = Array::zeros((shape[0], shape[1]));
                bisons.push(dfs(&arr, &mut reached, x_index, y_index));
            }
        }
    }
    bisons
}

fn largest_bisons(bisons: Vec<i32>) -> (i32, i32, i32) {
    let bisons = bisons.into_iter().sorted().rev().collect_vec();
    (bisons[0], bisons[1], bisons[2])
}

fn dfs(input: &Array2<i32>, reached: &mut Array2<i32>, x_index: usize, y_index: usize) -> i32 {
    let val = input[[y_index, x_index]];
    let size = input.shape();
    let [y_size, x_size] = [size[0], size[1]];
    let mut count = 1;
    reached[[y_index, x_index]] = 1;
    if y_index > 0 && input[[y_index-1, x_index]] != 9 && reached[[y_index-1, x_index]] == 0 && input[[y_index-1, x_index]] > val { count += dfs(&input, reached, x_index, y_index-1); }
    if x_index > 0 && input[[y_index, x_index-1]] != 9 && reached[[y_index, x_index-1]] == 0 && input[[y_index, x_index-1]] > val { count += dfs(&input, reached, x_index-1, y_index); }
    if y_index < y_size-1 && input[[y_index+1, x_index]] != 9 && reached[[y_index+1, x_index]] == 0 && input[[y_index+1, x_index]] > val { count += dfs(&input, reached, x_index, y_index+1); }
    if x_index < x_size-1 && input[[y_index, x_index+1]] != 9 && reached[[y_index, x_index+1]] == 0 && input[[y_index, x_index+1]] > val { count += dfs(&input, reached, x_index+1, y_index); }
    count
}

pub fn part_one(input: Vec<String>) -> i32 {
    let parsed = parse_input(input);
    count_smallest(parsed)
}

pub fn part_two(input: Vec<String>) -> i32 {
    let parsed = parse_input(input);
    let bisons = find_bisons(parsed);
    let bisons = largest_bisons(bisons);
    bisons.0 * bisons.1 * bisons.2
}

#[cfg(test)]
mod tests {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec![
            "2199943210".to_string(),
            "3987894921".to_string(),
            "9856789892".to_string(),
            "8767896789".to_string(),
            "9899965678".to_string(),
        ]
    }

    #[test]
    fn test_parse_input() {
        assert_eq!(parse_input(sample_input()), vec![
            [2, 1, 9, 9, 9, 4, 3, 2, 1, 0],
            [3, 9, 8, 7, 8, 9, 4, 9, 2, 1],
            [9, 8, 5, 6, 7, 8, 9, 8, 9, 2],
            [8, 7, 6, 7, 8, 9, 6, 7, 8, 9],
            [9, 8, 9, 9, 9, 6, 5, 6, 7, 8],
        ])
    }

    #[test]
    fn test_count_smallest() {
        let parsed = parse_input(sample_input());
        assert_eq!(count_smallest(parsed), 15);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input()), 15);
    }

    #[test]
    fn test_dfs() {
        let parsed = parse_input(sample_input());
        let arr = Array::from_shape_vec((parsed.len(), parsed[0].len()), parsed.into_iter().flatten().collect()).unwrap();
        let shape = arr.shape();
        let mut reached = Array::zeros((shape[0], shape[1]));
        assert_eq!(dfs(&arr, &mut reached,1, 0), 3);
        assert_eq!(dfs(&arr, &mut reached, 9, 0), 9);
        assert_eq!(dfs(&arr, &mut reached, 2, 2), 14);
    }

    #[test]
    fn test_find_bisons() {
        let parsed = parse_input(sample_input());
        assert_eq!(find_bisons(parsed), vec![3, 9, 14, 9]);
    }

    #[test]
    fn test_largest_bisons() {
        let parsed = parse_input(sample_input());
        let bisons = find_bisons(parsed);
        assert_eq!(largest_bisons(bisons), (14, 9, 9));
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input()), 1134);
    }
}
