#[derive(Clone, Eq, PartialEq, Copy)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Clone)]
struct Line {
    p1: Point,
    p2: Point,
}

fn parse_lines(input: &[String]) -> Vec<Line> {
    let mut vector = vec![];
    for line in input {
        let parts: Vec<&str> = line.split(" -> ").collect();
        let p1: Vec<&str> = parts[0].split(",").collect();
        let p2: Vec<&str> = parts[1].split(",").collect();
        let p1 = Point {
            x: p1[0].parse().unwrap(),
            y: p1[1].parse().unwrap(),
        };
        let p2 = Point {
            x: p2[0].parse().unwrap(),
            y: p2[1].parse().unwrap(),
        };
        vector.push(Line{p1, p2});
    }
    vector
}

fn is_vertical(line: Line) -> bool {
    line.p1.x == line.p2.x
}

fn is_horizontal(line: Line) -> bool {
    line.p1.y == line.p2.y
}

fn is_diagonal(line: Line) -> bool {
    (line.p1.x - line.p2.x).abs() == (line.p1.y - line.p2.y).abs()
}

fn filter_lines(lines: Vec<Line>) -> Vec<Line> {
    lines
        .into_iter()
        .filter(|line| is_horizontal(line.clone()) || is_vertical(line.clone()))
        .collect()
}

fn points_in_line(line: Line) -> Vec<Point> {
    let mut points = vec![];
    if is_horizontal(line.clone()) {
        let y = line.p1.y;
        if line.p1.x < line.p2.x {
            for x in line.p1.x..=line.p2.x {
                points.push(Point{x, y});
            }
        } else {
            for x in line.p2.x..=line.p1.x {
                points.push(Point{x, y});
            }
        }
    } else if is_vertical(line.clone()) {
        let x = line.p1.x;
        if line.p1.y < line.p2.y {
            for y in line.p1.y..=line.p2.y {
                points.push(Point{x, y});
            }
        } else {
            for y in line.p2.y..=line.p1.y {
                points.push(Point{x, y});
            }
        }
    } else if is_diagonal(line.clone()) {
        if line.p1.x < line.p2.x {
            if line.p1.y < line.p2.y {
                let mut new_p = line.p1;
                points.push(new_p);
                while new_p != line.p2 {
                    new_p.x += 1;
                    new_p.y += 1;
                    points.push(new_p);
                }
            } else {
                let mut new_p = line.p1;
                points.push(new_p);
                while new_p != line.p2 {
                    new_p.x += 1;
                    new_p.y -= 1;
                    points.push(new_p);
                }
            }
        } else {
            if line.p1.y < line.p2.y {
                let mut new_p = line.p1;
                points.push(new_p);
                while new_p != line.p2 {
                    new_p.x -= 1;
                    new_p.y += 1;
                    points.push(new_p);
                }
            } else {
                let mut new_p = line.p1;
                points.push(new_p);
                while new_p != line.p2 {
                    new_p.x -= 1;
                    new_p.y -= 1;
                    points.push(new_p);
                }
            }
        }
    }
    points
}

fn traverse_lines(lines: Vec<Line>) -> (Vec<Point>, Vec<i32>) {
    let mut points = vec![];
    let mut counts = vec![];

    for (_index, line) in lines.into_iter().enumerate() {
        for point in points_in_line(line) {
            match points.iter().position(|p| *p == point) {
                None => {
                    points.push(point);
                    counts.push(1);
                },
                Some(index) => {
                    counts[index] += 1;
                }
            }
        }
    }
    (points, counts)
}

fn count_multiples(counts: Vec<i32>) -> i32 {
    counts.into_iter().filter(|c| *c > 1).collect::<Vec<i32>>().len() as i32
}

pub fn part_one(input: &[String]) -> i32 {
    let parsed = parse_lines(input);
    let filtered = filter_lines(parsed);
    let (_points, counts) = traverse_lines(filtered);
    count_multiples(counts)
}

pub fn part_two(input: &[String]) -> i32 {
    let parsed = parse_lines(input);
    let (_points, counts) = traverse_lines(parsed);
    count_multiples(counts)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec![
            "0,9 -> 5,9".to_string(),
            "8,0 -> 0,8".to_string(),
            "9,4 -> 3,4".to_string(),
            "2,2 -> 2,1".to_string(),
            "7,0 -> 7,4".to_string(),
            "6,4 -> 2,0".to_string(),
            "0,9 -> 2,9".to_string(),
            "3,4 -> 1,4".to_string(),
            "0,0 -> 8,8".to_string(),
            "5,5 -> 8,2".to_string(),
        ]
    }

    #[test]
    fn test_parse_lines() {
        let parsed = parse_lines(sample_input().as_slice());
        assert_eq!(parsed[0].p1.x, 0);
        assert_eq!(parsed[5].p1.y, 4);
        assert_eq!(parsed[8].p2.y, 8);
    }

    #[test]
    fn test_filter_lines() {
        let parsed = parse_lines(sample_input().as_slice());
        assert_eq!(parsed.len(), 10);
        let filtered = filter_lines(parsed);
        assert_eq!(filtered.len(), 6);
    }

    #[test]
    fn test_traverse_lines_and_count() {
        let parsed = parse_lines(sample_input().as_slice());
        let filtered = filter_lines(parsed);
        let (_points, counts) = traverse_lines(filtered);
        let count = count_multiples(counts);

        assert_eq!(count, 5);
    }

    #[test]
    fn test_part_one() {
        let count = part_one(sample_input().as_slice());
        assert_eq!(count, 5);
    }

    #[test]
    fn test_part_two() {
        let count = part_two(sample_input().as_slice());
        assert_eq!(count, 12);
    }
}