fn parse(input: Vec<String>) -> Vec<i32> {
    input[0].split(",").map(|s| s.parse::<i32>().unwrap()).collect()
}

fn median(mut input: Vec<i32>) -> i32 {
    input.sort();
    if input.len() % 2 == 0 {
        let v1 = input[input.len() / 2];
        let v2 = input[input.len() / 2 - 1];
        return (v1 + v2) / 2;
    }
    input[input.len() / 2]
}

fn sum_differences(input: Vec<i32>, m: i32) -> i32 {
    input.into_iter().map(|v| (v - m).abs()).sum()
}

fn mean(input: Vec<i32>) -> i32 {
    let s: i32 = input.clone().into_iter().sum();
    let m: f64 = s as f64 / input.len() as f64;
    m.round() as i32
}

fn diffs(input: Vec<i32>, m: i32) -> i32 {
    let mut d = vec![];
    for val in input {
        let mut inner_sum = 0;
        for count in 1..=(val-m).abs() {
            inner_sum += count;
        }
        d.push(inner_sum)
    }
    d.into_iter().sum()
}

pub fn part_one(input: Vec<String>) -> i32 {
    let parsed = parse(input);
    let m = median(parsed.clone());
    sum_differences(parsed, m)
}

pub fn part_two(input: Vec<String>) -> i32 {
    let parsed = parse(input);
    let mut possible = vec![];
    let m = mean(parsed.clone());
    for p in m-1..=m+1 {
        possible.push(diffs(parsed.clone(), p));
    }
    *possible.iter().min().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec!["16,1,2,0,4,2,7,1,2,14".to_string()]
    }

    #[test]
    fn test_parse() {
        assert_eq!(parse(sample_input()), vec![16,1,2,0,4,2,7,1,2,14]);
    }

    #[test]
    fn test_find_median() {
        let parsed = parse(sample_input());
        let m = median(parsed.clone());
        assert_eq!(m, 2);
        let m = median([parsed, vec![3]].concat());
        assert_eq!(m, 2);
    }

    #[test]
    fn test_sum_differences() {
        let parsed = parse(sample_input());
        let m = median(parsed.clone());
        assert_eq!(sum_differences(parsed, m), 37);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input()), 37);
    }

    #[test]
    fn test_diffs() {
        let parsed = parse(sample_input());
        let m = mean(parsed.clone());
        assert_eq!(diffs(parsed.clone(), m), 168);
        let med = median(parsed.clone());
        assert_eq!(diffs(parsed.clone(), med), 206);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input()), 168);
    }
}
