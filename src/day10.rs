use itertools::Itertools;

enum CharType {
    Open,
    Close,
}

fn char_type(ch: char) -> CharType {
    let open_chars =  ['[', '(', '{', '<'];
    if open_chars.contains(&ch) { return CharType::Open; }
    CharType::Close
}

fn is_pair(open: char, close: char) -> bool {
    open == '[' && close == ']' ||
    open == '{' && close == '}' ||
    open == '(' && close == ')' ||
    open == '<' && close == '>'
}


fn is_corrupted(input: String) -> Option<char> {
    let mut stack: Vec<char> = Vec::new();
    for ch in input.chars() {
        match char_type(ch) {
            CharType::Open => stack.push(ch),
            CharType::Close => { 
                let old_val = stack.pop();
                if old_val.is_none() { return Some(ch); }
                let old_val = old_val.unwrap();
                if !is_pair(old_val, ch) {
                    return Some(ch);
                }
            }
        }
    }
    None
}

fn corruption_amount(input: String) -> i32 {
    match is_corrupted(input) {
        None => 0,
        Some(ch) => match ch {
            ')' => 3,
            ']' => 57,
            '}' => 1197,
            '>' => 25137,
            _ => 0,
        }
    }
}

fn incomplete_amount(ch: char) -> i32 {
    match ch {
        '(' => 1,
        '[' => 2,
        '{' => 3,
        '<' => 4,
        _ => 0,
    }
}

fn calcultate_incomplete_stack(mut stack: Vec<char>) -> i64 {
    let mut score: i64 = 0;
    while let Some(ch) = stack.pop() {
        score *= 5;
        score += incomplete_amount(ch) as i64;
    }
    score
}

fn filter_incomplete_lines(input: Vec<String>) -> Vec<String> {
    input.into_iter().filter(|v| is_corrupted(v.clone()).is_none()).collect_vec()
}

fn build_incomplete_stack(input: String) -> Vec<char> {
    let mut stack: Vec<char> = Vec::new();
    for ch in input.chars() {
        match char_type(ch) {
            CharType::Open => stack.push(ch),
            CharType::Close => { stack.pop(); },
        }
    }
    stack
}

fn all_totals(input: Vec<String>) -> Vec<i64> {
    let incompletes = filter_incomplete_lines(input);
    incompletes.into_iter().map(|v| calcultate_incomplete_stack(build_incomplete_stack(v.to_string()))).collect_vec()
}

pub fn part_one(input: Vec<String>) -> i32 {
    input.into_iter().fold(0, |acc, val| acc + corruption_amount(val))
}

pub fn part_two(input: Vec<String>) -> i64 {
    let mut totals = all_totals(input);
    totals.as_mut_slice().sort_unstable();
    totals[totals.len() / 2]
}

#[cfg(test)]
mod tests {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec![
            r"[({(<(())[]>[[{[]{<()<>>".to_string(),
            r"[(()[<>])]({[<{<<[]>>(".to_string(),
            r"{([(<{}[<>[]}>{[]{[(<()>".to_string(),
            r"(((({<>}<{<{<>}{[]{[]{}".to_string(),
            r"[[<[([]))<([[{}[[()]]]".to_string(),
            r"[{[{({}]{}}([{[{{{}}([]".to_string(),
            r"{<[[]]>}<{[{[{[]{()[[[]".to_string(),
            r"[<(<(<(<{}))><([]([]()".to_string(),
            r"<{([([[(<>()){}]>(<<{{".to_string(),
            r"<{([{{}}[<[[[<>{}]]]>[]]".to_string(),
        ]
    }

    #[test]
    fn test_is_corrupted() {
        let issue_char = is_corrupted(sample_input()[2].clone());
        assert_eq!(is_corrupted(sample_input()[0].clone()), None);
        assert_eq!(issue_char, Some('}'));
    }

    #[test]
    fn test_corruption_amount() {
        assert_eq!(corruption_amount(sample_input()[2].clone()), 1197);
        assert_eq!(corruption_amount(sample_input()[0].clone()), 0);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input()), 26397);
    }

    #[test]
    fn test_incomplete_amount() {
        assert_eq!(incomplete_amount('('), 1);
        assert_eq!(incomplete_amount('['), 2);
        assert_eq!(incomplete_amount('{'), 3);
        assert_eq!(incomplete_amount('<'), 4);
    }

    #[test]
    fn test_calculate_incomplete_stack() {
        assert_eq!(calcultate_incomplete_stack(vec!['<', '{', '(', '[']), 294);
    }

    #[test]
    fn test_filter_incomplete_lines() {
        assert_eq!(filter_incomplete_lines(sample_input()), vec![
            r"[({(<(())[]>[[{[]{<()<>>",
            r"[(()[<>])]({[<{<<[]>>(",
            r"(((({<>}<{<{<>}{[]{[]{}",
            r"{<[[]]>}<{[{[{[]{()[[[]",
            r"<{([{{}}[<[[[<>{}]]]>[]]",
        ]);
    }

    #[test]
    fn test_build_incomplete_stack() {
        assert_eq!(build_incomplete_stack(r"[({(<(())[]>[[{[]{<()<>>".to_string()), vec!['[', '(', '{', '(', '[', '[', '{', '{']);
    }

    #[test]
    fn test_all_totals() {
        assert_eq!(all_totals(sample_input()), vec![288957, 5566, 1480781, 995444, 294]);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input()), 288957);
    }
}

