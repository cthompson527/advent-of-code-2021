fn increase_depth(input: &[i32]) -> Vec<i32> {
    let vals = input.iter();
    let next_vals = input.iter().skip(1);

    vals
        .zip(next_vals)
        .filter(|(cur, next)| next > cur)
        .map(|(_cur, &next)| next)
        .collect()
}

fn summing_window(input: &[i32]) -> Vec<i32> {
    let vals = input.iter();
    let next_vals = input.iter().skip(1);
    let after_next = input.iter().skip(2);

    vals
        .zip(next_vals)
        .zip(after_next)
        .map(|(first_two, after)| first_two.0 + first_two.1 + after)
        .collect()
}

pub fn part_one(input: &[i32]) -> i32 {
    increase_depth(input).len() as i32
}

pub fn part_two(input: &[i32]) -> i32 {
    increase_depth(summing_window(input).as_slice()).len() as i32
}

#[cfg(test)]
mod tests {
    use super::summing_window;
    use super::increase_depth;
    use super::part_one;
    use super::part_two;

    fn sample_input() -> &'static[i32; 10] {
         &[ 199, 200, 208, 210, 200, 207, 240, 269, 260, 263 ]
    }

    #[test]
    fn test() {
        assert_eq!(
            increase_depth(sample_input()),
            vec![200, 208, 210, 207, 240, 269, 263]
        );
    }

    #[test]
    fn empty() {
        assert_eq!(increase_depth(&[]), vec![]);
        assert_eq!(increase_depth(&[1i32]), vec![]);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input()), 7);
    }

    #[test]
    fn test_create_summing_window() {
        assert_eq!(
            summing_window(sample_input()),
            vec![607, 618, 618, 617, 647, 716, 769, 792]
        );
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input()), 5);
    }
}
