fn ones(s: &str) -> Vec<i32> {
    s
        .chars()
        .map(|c|
            c.to_digit(10).unwrap() as i32)
        .collect()
}

fn sum_v(v: Vec<i32>, u: Vec<i32>) -> Vec<i32> {
    v
        .iter()
        .zip(u)
        .map(|(v_val, u_val)| v_val + u_val)
        .collect()
}

fn find_most_common(input: &[String]) -> String {
    let init_vec = vec![0; input[0].len()];
    let len_report = input.len();
    input
        .iter()
        .map(|s| ones(s.as_str()))
        .fold(init_vec, sum_v)
        .iter()
        .map(|val| ((*val as f64 / len_report as f64).round() as i32).to_string())
        .collect::<String>()
}

fn find_least_common(input: &[String]) -> String {
    let most_common = find_most_common(input);
    most_common
        .chars()
        .map(|c| if c == '0' { '1' } else { '0' })
        .collect()
}

fn gamma(input: &[String]) -> i32 {
    let most_common = find_most_common(input);
    i32::from_str_radix(most_common.as_str(), 2).unwrap()
}

fn epsilon(gamma: i32) -> i32 {
    const fn num_bits<T>() -> usize { std::mem::size_of::<T>() * 8 }
    let log = num_bits::<i32>() as u32 - gamma.leading_zeros();
    let mask = (1 << log) - 1;

    !gamma & mask
}

fn same_character_at_index(s1: String, s2: String, index: usize) -> bool {
    s1.as_bytes()[index] == s2.as_bytes()[index]
}

fn oxygen_rating(input: &[String]) -> i32 {
    fn common_substring(input: &[String], index: usize) -> String {
        let most_common = find_most_common(input);
        if index > most_common.len() {
            panic!("Not found!!");
        }
        let found_strings: Vec<String> = input
            .iter()
            .map(|c| c.clone())
            .filter(|cur| same_character_at_index(cur.clone(), most_common.clone(), index))
            .collect::<Vec<String>>();
        if found_strings.len() == 1 {
            found_strings[0].clone()
        } else {
            common_substring(found_strings.as_slice(), index + 1)
        }
    }
    i32::from_str_radix(common_substring(input, 0).as_str(), 2).unwrap()
}

fn co2_scrubber(input: &[String]) -> i32 {
    fn common_substring(input: &[String], index: usize) -> String {
        let least_common = find_least_common(input);
        if index > least_common.len() {
            panic!("Not found!!");
        }
        let found_strings: Vec<String> = input
            .iter()
            .map(|c| c.clone())
            .filter(|cur| same_character_at_index(cur.clone(), least_common.clone(), index))
            .collect::<Vec<String>>();
        if found_strings.len() == 1 {
            found_strings[0].clone()
        } else {
            common_substring(found_strings.as_slice(), index + 1)
        }
    }
    i32::from_str_radix(common_substring(input, 0).as_str(), 2).unwrap()
}

pub fn part_one(input: &[String]) -> i32 {
    let g = gamma(input);
    g * epsilon(g)
}

pub fn part_two(input: &[String]) -> i32 {
    let oxygen = oxygen_rating(input);
    let co2 = co2_scrubber(input);
    oxygen * co2
}


#[cfg(test)]
mod tests {
    use super::*;

    fn sample_input() -> Vec<String> {
        vec![
            "00100".to_string(),
            "11110".to_string(),
            "10110".to_string(),
            "10111".to_string(),
            "10101".to_string(),
            "01111".to_string(),
            "00111".to_string(),
            "11100".to_string(),
            "10000".to_string(),
            "11001".to_string(),
            "00010".to_string(),
            "01010".to_string(),
        ]
    }

    #[test]
    fn test_ones() {
        assert_eq!(ones(sample_input()[0].as_str()), vec![0, 0, 1, 0, 0]);
    }

    #[test]
    fn test_sum_two_vectors() {
        let v = vec![1, 2, 3, 4, 5];
        let u = vec![2, 3, 4, 5, 6];
        assert_eq!(sum_v(v, u), vec![3, 5, 7, 9, 11]);
    }

    #[test]
    fn test_gamma() {
        assert_eq!(gamma(sample_input().as_slice()), 22);
    }

    #[test]
    fn test_epsilon() {
        assert_eq!(epsilon(22), 9);
    }

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(sample_input().as_slice()), 198);
    }

    #[test]
    fn test_oxygen_rating() {
        assert_eq!(oxygen_rating(sample_input().as_slice()), 23);
    }

    #[test]
    fn test_co2_scrubber() {
        assert_eq!(co2_scrubber(sample_input().as_slice()), 10);
    }

    #[test]
    fn test_same_character_at_index() {
        let s1 = "0110001".to_string();
        let s2 = "0111011".to_string();
        assert_eq!(same_character_at_index(s1.clone(), s2.clone(), 2), true);
        assert_eq!(same_character_at_index(s1, s2, 3), false);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(sample_input().as_slice()), 230);
    }
}

