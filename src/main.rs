use std::process::exit;
use structopt::StructOpt;

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod readfile;

#[derive(Debug, StructOpt)]
#[structopt(name = "AoC2021", about = "The answers to my Advent of Code")]
struct Cli {
    #[structopt(short, long)]
    day: i32,

}

fn input<T>(day: i32) -> Vec<T>
where T: std::str::FromStr, <T as std::str::FromStr>::Err : std::fmt::Debug
{
    let filename = format!("inputs/input{}.txt", day);
    readfile::read::<T>(filename)
}

fn main() {
    let args = Cli::from_args();
    let (p_one_ans, p_two_ans): (i64, i64) = match args.day {
        1 => {
            let i = input::<i32>(1);
            (day1::part_one(i.as_slice()) as i64, day1::part_two(i.as_slice()) as i64)
        },
        2 => {
            let i = input::<String>(2);
            (day2::part_one(i.as_slice()) as i64, day2::part_two(i.as_slice()) as i64)
        },
        3 => {
            let i = input::<String>(3);
            (day3::part_one(i.as_slice()) as i64, day3::part_two(i.as_slice()) as i64)
        },
        4 => {
            let i = input::<String>(4);
            (day4::part_one(i.as_slice()) as i64, day4::part_two(i.as_slice()) as i64)
        },
        5 => {
            let i = input::<String>(5);
            (day5::part_one(i.as_slice()) as i64, day5::part_two(i.as_slice()) as i64)
        },
        6 => {
            let i = input::<String>(6);
            (day6::part_one(i.as_slice()) as i64, day6::part_two(i.as_slice()))
        },
        7 => {
            let i = input::<String>(7);
            (day7::part_one(i.clone()) as i64, day7::part_two(i) as i64)
        },
        8 => {
            let i = input::<String>(8);
            (day8::part_one(i.clone()) as i64, day8::part_two(i) as i64)
        },
        9 => {
            let i = input::<String>(9);
            (day9::part_one(i.clone()) as i64, day9::part_two(i) as i64)
        },
        10 => {
            let i = input::<String>(10);
            (day10::part_one(i.clone()) as i64, day10::part_two(i) as i64)
        },
        _ => {
            println!("Invalid day input: {}", args.day);
            exit(-1);
        }
    };
    println!("The answer for day {} - part 1 is: {}", args.day, p_one_ans);
    println!("The answer for day {} - part 2 is: {}", args.day, p_two_ans);
}
